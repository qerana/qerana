<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15" />
        <meta name="lang" content="es" />
        <meta name="author" content="qerana" />
        <meta name="organization" content="qerana" />
        <meta name="locality" content="qworld" />
        <title>Error <?php echo $code_status; ?></title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <link href="/_styles/sbadmin2/css/sb-admin-2.min.css" rel="stylesheet">
    </head>
    <body style="background-color: #F8F8F8;" >

        <div class="container-fluid">

            <div class="card mb-4 ">
                <div class="card-header">
                    <div class="text-center">
                        <div class="error mx-auto small" data-text="<?php echo $code_status; ?>"><?php echo $code_status; ?></div><br>
                        <p class="text-gray-500 mt-0 pt-0"><?php echo $message; ?></p><hr>
                        <button class="btn btn-secondary"  onclick='history.back()'>Volver</button>
                    </div>
                </div>
            </div>

        </div>

    </body>
</html>
<?php
die();
