<?php
/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Cliqer;

use Qerapp\qbasic\model\QBasicService,
    Qerapp\qbasic\model\modeling\ModelService,
    Qerapp\qbasic\model\modeling\EntityService;

/**
 * *****************************************************************************
 * Cliqer
 * *****************************************************************************
 * @author diemarc
 * 
 * Is a some kind of FrontController, but only for CLI mode.
 * Performce devtools taks, and another cli calls, like  a cron job.
 * 
 * *****************************************************************************
 */
class Cliqer
{
    private

    /** service to run */
        $_service_module,
        // service to run
        $_service_run,
        /** action service to run */
        $_action_to_run,
        /** array of options to pass to service method */
        $_options,
        // reserved actions for qbasic
        $_qbasic_keywords = [
            'module', 'controller', 'model', 'entity', 'view', 'service', 'relate'
    ];

    public function __construct()
    {
        // only cli petition
        if (PHP_SAPI != 'cli') {
            \QException\Exceptions::showError('Cliqer.Error ',
                'Cant access to cliqer from http request');
            die();
        }
    }

    /**
     * -------------------------------------------------------------------------
     * run cliqer, this will called in bash file
     * -------------------------------------------------------------------------
     * @param type $components
     */
    public function run($components, $options_naked = '')
    {
        // echo "====================================== \n"
        // . "               CliQer  \n";
        // para los espacios en blanco
        $options = str_replace('%', ' ', $options_naked);

        (!empty($options)) ? $this->_setOptions($options) : '';

        try {
            $this->_init($components);
            // dispatch
            $this->_dispatch();
        } catch (\Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /**
     * -------------------------------------------------------------------------
     * dispatch to service 
     * -------------------------------------------------------------------------
     */
    private function _dispatch()
    {

        if ($this->_service_module === 'qbasic') {

            echo "          QBasic $this->_service_run  \n"
            ."______________________________________\n"
            ."Task:$this->_action_to_run \n \n"
            ."Message: \n";

            $Dev = new QBasicService($this->_service_run, $this->_action_to_run,
                $this->_options);
            $Dev->develop();
            echo "\n--DONE--\n\n\n";
            echo "---------------THE END----------------\n\n";
        }
        // access
        else {

            return $this->dispatchCliqerService();

            // find model to locate service namespace
            $method = $this->_action_to_run;



            $ModelService = new ModelService();
            $ModelService->setModel($this->_service_run);


            if (!$ModelService->Model) {
                echo "\n";
                echo 'Service '.$this->_service_run.' not found!!'."\n";
                die();
            }
            $service_ns = '\\'.$ModelService->Model->model_namespace.'\\'.ucfirst($this->_service_run).'Cliqer';



            try {
                $ObjectService = new $service_ns($this->_options);
                $ObjectService->$method();
            } catch (\Exception $ex) {
                echo "\n".'not found cliqer mutator!!!';
                echo $ex->getMessage();
            }
        }
    }

    /**
     * Crea el namespace para el cliqer service, el anterior
     * buscaba en la entidad modelo, este busca en la entidad entity
     */
    private function dispatchCliqerService()
    {

        // find model to locate service namespace
        $method = $this->_action_to_run;


        $EntityService = new EntityService();
        $EntityService->set_entity_name(ucfirst($this->_service_run));
        $EntityInfo    = $EntityService->getEntityInfo();

        if (is_null($EntityInfo)) {
            echo "\n";
            echo 'No se encuentra el service asociado a la entidad '.$this->_service_run.'. Tienes que llamar a tu cliqer "NombreEntidadCliqer"'."\n";
            die();
        }

        $service_ns = '\\'.$EntityInfo->Model->model_namespace.'\\'.ucfirst($this->_service_run).'Cliqer';

       
            $ObjectService = new $service_ns($this->_options);
            $ObjectService->$method();
       
    }

    /**
     * -------------------------------------------------------------------------
     * Initialize and set parameters
     * -------------------------------------------------------------------------
     * @param type $components
     * @return type
     */
    private function _init($components)
    {
        // set the action to run
        $components_array = explode(':', $components);

        if (!isset($components_array[1])) {
            throw new \RuntimeException('Yo must specified a action with :'."\n");
        }


        // if service_module, (array 0 form components_array is in _service_devtools keywords)
        // then use qbasic
        $this->_service_module = (in_array($components_array[0],
                $this->_qbasic_keywords)) ? 'qbasic' : $components_array[0];
        $this->_service_run    = $components_array[0];

        // set acction
        $this->_action_to_run = $components_array[1];
    }

    /**
     * -------------------------------------------------------------------------
     * Set options
     * -------------------------------------------------------------------------
     * @param type $options
     */
    private function _setOptions($options = '')
    {
        // set options
        $options_array = explode('--', $options);
        unset($options_array[0]);


        foreach ($options_array AS $k => $v):
            $value_array                     = explode('=', $v);
            $this->_options[$value_array[0]] = $value_array[1];

        endforeach;
    }
}
